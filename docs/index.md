# OpenFlexure Block Stage

![](images/blockstage_render.png)

A 3D printed *x-y-z* translation stage.

## To build the stage you will need
{{BOM}}


[band tool]:tools.md#bandtool  "{cat: PrintedTool}"
[nut tool]:tools.md#nuttool  "{cat: PrintedTool}"

[base]: models/base.stl "{cat:3DPrinted}"
[main body]: models/main_body.stl "{cat:3DPrinted}"
[moving platform]: models/moving_platform.stl "{cat:3DPrinted}"
[large gears]: models/gears.stl "{cat:3DPrinted}"


[2.5mm Allen key]: "{cat:tool}"
[light oil]: "{cat:tool}"

![](images/0-1-AllParts.jpg)


## Printing the parts
Before you start you should print all the 3D printed parts.

## Cut the ties

On the [main body]{Qty: 1} there are small plastic ties which help the actuator columns stay aligned when printing. Use [precision wire cutters]{Qty:1} to cut these ties. There are two ties to cut inside each column.  
![](images/1-1-Ties.jpg)![](images/1-2-Cut.jpg)![](images/1-3-NoTies.jpg)

[precision wire cutters]: "{cat:Tool}"

## Attach Base
Attach the [main body] to the [base]{Qty: 1} via the little lugs using three [M3x6mm screws][M3x6mm screw]{Qty: 3}.
![](images/2-1-Base.jpg)![](images/2-2-Base.jpg)

[M3x6mm screw]: Hardware.yaml#CapScrew_M3x6mm_SS

## Assembling an actuator


![](images/3-1-Actuator.jpg)

Insert a [brass M3 nut]{Qty: 3} into the slot on one of the actuator columns of the main body (this is sometimes made easier with the [nut tool]{Qty: 1}. The nut tool is the shorter of the two tools and is missing from the above picture.
[nut tool]{Qty: 1}

![](images/3-2-Nut.jpg)![](images/3-3-Nut.jpg)

[brass M3 nut]: Hardware.yaml#Nut_M3_Brass

Take a [large gears]{Qty: 3} and push a [M3x25mm hex head screw]{Qty: 3} into it. Put **two** [washers][M3 washers]{Qty: 6} on the screw.

![](images/3-4-Washers.jpg)

[M3x25mm hex head screw]: Hardware.yaml#HexBolt_M3x25mm_SS
[M3 washers]: Hardware.yaml#Washer_M3_SS

Start screwing the gear into the actuator column from the top. Apply a small amount of [light oil]{Qty: "A few drops of"} to the screw thread, before you fully tighten the screw  
![](images/3-5-GearAttach.jpg)![](images/3-6-Oil.jpg)![](images/3-7-GearAttach.jpg)

*Now comes the trickiest part. Once you get the knack this is easy, but it takes a few goes.*

Look through the hole you pushed the nut through, rotate the gear above until you see the hole inside align with it. Push the [nut tool]{qty:1} into the hole so the column cant move.

![](images/4-2-NutTool.jpg)

Bend the [band tool]{Qty:1} ready for use.  
![](images/4-1-BandTool.jpg)

Turn the stage upside down and hold an [O-ring][Viton O-ring 30x2mm]{Qty: 3} over the holes in the bottom of the actuator column.
[Viton O-ring 30x2mm]: "{note: ' - You might want some spares!'}"

![](images/4-3-Band.jpg)

Push the [band tool]{Qty:1} onto the two sides of the o-ring so that it begins to push the o-ring into the actuator column. Make sure that all both groves/hooks on each side of the tool are holding the band.

![](images/4-4-Hook.jpg)

Now you want to push the tool into the leg with your thumbs, without letting the nut tool slip out. If the nut tool is too annoying you can also lock the column by  holding the gear (and screw) tight against the stage. You have to push pretty hard (normally requires both hands) but once you hear a satisfying click you should be done.

![](images/4-5-Push.jpg)![](images/4-6-Push.jpg)

Remove the band tool. If it was successful the band should be held solidly in the actuator with both sides of the band neatly sitting in the grove. They need a bit of positioning. If you have enough light you can see that the bands are held onto small hooks inside the actuator column. If this didn't work the first time you can pull the band out and try again. If the bands are snapping it is probably a sign that you are not locking the actuator column with the nut tool.

![](images/4-7-Inserted.jpg)![](images/4-8-Inspect.jpg)

Repeat this process for all three actuators.

![](images/4-9-Complete.jpg)

## Tapping the holes at the sides of the the stages

The holes on the fixed stage are somewhat triangular. This is to allow a normal machine screw to easily make a thread. **Do not use a normal M3 tap, it will sit too low in the hole and damage the plastic.**

Tap a hole (make the tread) on the moving stage using a normal [M3x6mm screw] by screwing a screw into it. Make sure to hold the screw vertical and then drive it into the plastic until it reaches the bottom. **Do not over torque the screw or you will damage the plastic.** You can now remove the screw.

Repeat for all eight holes on the fixed stage.  
![](images/5-1-Tap.jpg)![](images/5-2-Tap.jpg)

Repeat this process to tap the 8 holes at the edge of the [moving platform]{Qty: 1}.

## Attaching the moving platform
 
Take the [moving platform] and place it onto the centre of the stage so that the slot aligns with the slot on the moving platform, and the four central holes align with holes in the mechanism.

![](images/6-1-Align.jpg)

Attach the platform to the stage with four [M3x6mm screws][M3x6mm screw]{Qty: 4}, this requires a [2.5mm Allen key]{Qty: 1}.

![](images/6-2-Screw.jpg)![](images/6-3-Complete.jpg)

## Attaching motors - Optional

[small gears]: "{cat:3DPrinted, note: '- Only if using motors'}"
[28BYJ-48 micro geared stepper motors]: "{note: '- Only if using motors'}"

Push a [small printed gear][small gears]{Qty: 3} onto each [stepper motor][28BYJ-48 micro geared stepper motors]{Qty: 3}.

Attach motors to the main body such that the gears mesh with the gears on the actuators. Use two [M4x6mm button head screws]{Qty: 6} per motor (requires [2.5mm Allen key]{Qty: 1}) to secure them in place.

[M4x6mm button head screws]: Hardware.yaml#ButtonScrew_M4x6mm_SS

## Completed

Your stage is now ready to use in the lab.

![](images/7-1-Lab.jpg)


