#!/bin/bash

mkdir -p builds
openscad -o builds/actuator_assembly_tools.stl openscad/actuator_assembly_tools.scad
openscad -o builds/base.stl openscad/base.scad
openscad -o builds/main_body.stl openscad/main_body.scad
openscad -o builds/moving_platform.stl openscad/moving_platform.scad
openscad -o builds/gears.stl openscad/gears.scad

